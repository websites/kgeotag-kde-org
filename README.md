# KGeoTag

This is KGeoTag's homepage.

## Build instruction

```
gem install bundler jekyll
bundle config set --local path vendor/bundle
bundle install
```

## Run development

```
bundle exec jekyll serve
```

## Run production

```
bundle exec jekyll build
```

## License:

* Content (text, image) are licensed under CC-BY-SA-4.0
* Code is licensed under AGPL-3.0-or-later

See LICENSES folder and [KDE licensing policy](https://community.kde.org/Policies/Licensing_Policy) for more information.
